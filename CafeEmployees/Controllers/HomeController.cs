﻿using System.Web.Mvc;

namespace CafeEmployees.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}