﻿using CafeEmployees.Models;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace CafeEmployees.Models
{
    public class CafeRepository
    {
        private static readonly string ConnectionString = @"Data Source=MacBook-Air-Olga.local;Initial Catalog=cafe_employees;Integrated Security=True";
        public static List<EmployeeModel> GetEmployees()
        {
            using (IDbConnection db = new SqlConnection(ConnectionString))
            {
                return db.Query<EmployeeModel>("SELECT * FROM Employees").ToList();
            }
        }

        public static EmployeeModel Get(int id)
        {
            using (IDbConnection db = new SqlConnection(ConnectionString))
            {
                return db.Query<EmployeeModel>("SELECT * FROM Employees WHERE Id = @id", new { id }).FirstOrDefault();
            }
        }

        public static void Create(EmployeeModel employee)
        {
            using (IDbConnection db = new SqlConnection(ConnectionString))
            {
                var sqlQuery = "INSERT INTO Employees (Name, Age) VALUES(@Name, @Age)";
                db.Execute(sqlQuery, employee);

                // если мы хотим получить id добавленного пользователя
                //var sqlQuery = "INSERT INTO Users (Name, Age) VALUES(@Name, @Age); SELECT CAST(SCOPE_IDENTITY() as int)";
                //int? employeeId = db.Query<int>(sqlQuery, employee).FirstOrDefault();
                //employee.Id = employeeId.Value;
            }
        }

        public static void Update(EmployeeModel employee)
        {
            using (IDbConnection db = new SqlConnection(ConnectionString))
            {
                var sqlQuery = "UPDATE Employees SET Name = @Name, Age = @Age WHERE Id = @Id";
                db.Execute(sqlQuery, employee);
            }
        }

        public static void Delete(int id)
        {
            using (IDbConnection db = new SqlConnection(ConnectionString))
            {
                var sqlQuery = "DELETE FROM Employees WHERE Id = @id";
                db.Execute(sqlQuery, new { id });
            }
        }
    }
}