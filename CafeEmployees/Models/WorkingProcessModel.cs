using System;

namespace CafeEmployees.Models
{
    public class WorkingProcessModel
    {
        private int Id { get; set; }
        private int EmployeeId { get; set; }
        private DateTime Date { get; set; }
        private DateTime StartTime { get; set; }
        private DateTime EndTime { get; set; }
    }
}