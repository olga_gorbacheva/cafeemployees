using System;

namespace CafeEmployees.Models
{
    public class EmployeeModel
    {
        private int Id { get; set; }
        private string Name { get; set; }
        private string Surname { get; set; }
        private DateTime DataOfBirth { get; set; }
        private int Phone { get; set; }
        private int Passport { get; set; }
        private int QualificationId { get; set; }
    }
}